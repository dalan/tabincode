use anyhow::{bail, Result};
use clap::{Args, Parser};
use std::path::PathBuf;

mod converter;
mod importer;
mod symbols;

/// Convert a tabuar from csv/xlsx/ods to text
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Options {
    /// The input filename
    #[clap(short, long)]
    input: PathBuf,
    /// The csv options
    #[clap(flatten)]
    csv: CsvOption,
    /// The spreadsheet options
    #[clap(flatten)]
    spreadsheet: SpreadsheetOption,
    /// The convert options
    #[clap(flatten)]
    convert: converter::Option,
}

/// CSV options
#[derive(Args, Debug)]
struct CsvOption {
    /// The csv delimiter
    #[clap(short, long, default_value_t = ',')]
    delimiter: char,
}

/// Spreadsheet options
#[derive(Args, Debug)]
struct SpreadsheetOption {
    /// The spreadsheet page
    #[clap(short, long)]
    page: Option<String>,
}

fn main() -> Result<()> {
    let args = Options::parse();

    if let Some(kind) = infer::get_from_path(&args.input)? {
        match kind.mime_type() {
            "application/vnd.oasis.opendocument.spreadsheet"
            | "application/vnd.ms-excel"
            | "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => {
                let spreadsheet_importer =
                    importer::Spreadsheets::new(args.input, args.spreadsheet.page.as_deref())?;
                let converter = converter::Converter::new(spreadsheet_importer, args.convert)?;
                println!("{}", converter);
            }
            _ => bail!("Invalid application file type"),
        }
    } else {
        let csv_importer = importer::Csv::new(args.input, args.csv.delimiter)?;
        let converter = converter::Converter::new(csv_importer, args.convert)?;
        println!("{}", converter);
    }

    Ok(())
}
