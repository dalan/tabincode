use crate::symbols;
use anyhow::{anyhow, Result};
use clap::{Args, ArgEnum};
use pad::PadStr;
use std::cmp::Ordering;
use std::fmt::Display;
use unicode_width::UnicodeWidthStr;

/// An alignment
#[derive(PartialEq, Eq, Debug, Copy, Clone, ArgEnum)]
pub enum Alignment {
    /// Text on the left, spaces on the right.
    Left,
    /// Text on the right, spaces on the left.
    Right,
    /// Text in the middle, spaces around it, but **shifted to the left** if it can’t be exactly central.
    Center,
}

impl Default for Alignment {
    fn default() -> Self {
        Alignment::Center
    }
}

impl Into<pad::Alignment> for Alignment {
    fn into(self) -> pad::Alignment {
        match self {
            Alignment::Left => pad::Alignment::Left,
            Alignment::Center => pad::Alignment::Middle,
            Alignment::Right => pad::Alignment::Right,
        }
    }
}

/// Convert options
#[derive(Args, Debug)]
pub struct Option {
    /// Use ascii only characters
    #[clap(short, long)]
    ascii: bool,
    /// Column header
    #[clap(short, long, default_value = "0")]
    column_header: usize,
    /// Line header
    #[clap(short, long, default_value = "1")]
    line_header: usize,
    /// Header alignment
    #[clap(short='w', long, arg_enum, default_value_t)]
    alignment_header: Alignment,
    /// Data alignment
    #[clap(short='t', long, arg_enum, default_value_t)]
    alignment_data: Alignment,
}

pub struct Converter {
    datas: Vec<Vec<String>>,
    option: Option,
}

impl Converter {
    pub fn new<I: Iterator<Item = Vec<String>>>(it: I, option: Option) -> Result<Self> {
        let datas: Vec<Vec<String>> = it.collect();
        if datas.is_empty() {
            Err(anyhow!("Empty tabular"))
        } else {
            Ok(Converter { datas, option })
        }
    }

    fn vertical_symbol(
        &self,
        symbol_set: &symbols::Set,
        line_index: usize,
        column_index: usize,
    ) -> &str {
        if line_index < self.option.line_header {
            symbol_set.vertical_header
        } else {
            match (column_index + 1).cmp(&self.option.column_header) {
                Ordering::Equal => symbol_set.vertical_header,
                Ordering::Greater => symbol_set.vertical,
                Ordering::Less => symbol_set.vertical_header,
            }
        }
    }

    fn print_line(
        &self,
        f: &mut std::fmt::Formatter<'_>,
        sizes: &[usize],
        symbol_set: &symbols::Set,
        line: LineType,
    ) -> std::fmt::Result {
        let (
            left_symbol,
            left_header_symbol,
            normal_symbol,
            normal_header_symbol,
            middle_header_symbol,
            middle_last_header_symbol,
            middle_symbol,
            right_symbol,
            right_header_symbol,
        ) = match line {
            LineType::FIRST => {
                // Get line types
                let line_type = if self.option.line_header > 0 {
                    symbols::Direction::HEADER
                } else {
                    symbols::Direction::NORMAL
                };

                (
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::NONE,
                        right: line_type,
                        up: symbols::Direction::NONE,
                        down: line_type,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::NONE,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::HEADER,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: line_type,
                        right: line_type,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::HEADER,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: line_type,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::HEADER,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: line_type,
                        right: line_type,
                        up: symbols::Direction::NONE,
                        down: line_type,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: line_type,
                        right: symbols::Direction::NONE,
                        up: symbols::Direction::NONE,
                        down: line_type,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::NONE,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::HEADER,
                    }),
                )
            }
            LineType::LAST => {
                // Get line types
                let line_type = if self.datas.len() <= self.option.line_header {
                    symbols::Direction::HEADER
                } else {
                    symbols::Direction::NORMAL
                };

                (
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::NONE,
                        right: line_type,
                        up: line_type,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::NONE,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: line_type,
                        right: line_type,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: line_type,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: line_type,
                        right: line_type,
                        up: line_type,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: line_type,
                        right: symbols::Direction::NONE,
                        up: line_type,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::NONE,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::NONE,
                    }),
                )
            }
            LineType::MIDDLE(line) => {
                // Get lines type
                let (up_line_type, down_line_type) = match (line + 1).cmp(&self.option.line_header)
                {
                    Ordering::Less => (symbols::Direction::HEADER, symbols::Direction::HEADER),
                    Ordering::Equal => (symbols::Direction::HEADER, symbols::Direction::NORMAL),
                    Ordering::Greater => (symbols::Direction::NORMAL, symbols::Direction::NORMAL),
                };

                (
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::NONE,
                        right: up_line_type,
                        up: up_line_type,
                        down: down_line_type,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::NONE,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::HEADER,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: up_line_type,
                        right: up_line_type,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::NONE,
                        down: symbols::Direction::NONE,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::HEADER,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::HEADER,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: up_line_type,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::HEADER,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: up_line_type,
                        right: up_line_type,
                        up: up_line_type,
                        down: down_line_type,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: up_line_type,
                        right: symbols::Direction::NONE,
                        up: up_line_type,
                        down: down_line_type,
                    }),
                    symbol_set.get(symbols::Symbol {
                        left: symbols::Direction::HEADER,
                        right: symbols::Direction::NONE,
                        up: symbols::Direction::HEADER,
                        down: symbols::Direction::HEADER,
                    }),
                )
            }
        };
        write!(
            f,
            "{}",
            if self.option.column_header > 0 {
                left_header_symbol
            } else {
                left_symbol
            }
        )?;
        let mut size_it = sizes.iter().enumerate().peekable();
        while let Some((column, size)) = size_it.next() {
            // Get horizontal symbol
            let horizontal_symbol = if column < self.option.column_header {
                normal_header_symbol
            } else {
                normal_symbol
            };

            write!(f, "{}", horizontal_symbol.repeat(*size))?;
            if size_it.peek().is_some() {
                write!(
                    f,
                    "{}",
                    match (column + 1).cmp(&self.option.column_header) {
                        Ordering::Less => middle_header_symbol,
                        Ordering::Equal => middle_last_header_symbol,
                        Ordering::Greater => middle_symbol,
                    }
                )?;
            }
        }
        writeln!(
            f,
            "{}",
            if self.option.column_header >= self.datas[0].len() {
                right_header_symbol
            } else {
                right_symbol
            }
        )?;
        Ok(())
    }
}

impl Display for Converter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Get size
        let mut sizes = vec![0; self.datas[0].len()];
        for line in &self.datas {
            for (index, data) in line.iter().enumerate() {
                sizes[index] = usize::max(sizes[index], data.width());
            }
        }

        let symbols = if self.option.ascii {
            symbols::ASCII
        } else {
            symbols::NORMAL
        };

        // Print
        self.print_line(f, &sizes, &symbols, LineType::FIRST)?;
        let mut line_it = self.datas.iter().enumerate().peekable();
        while let Some((line_index, line)) = line_it.next() {
            write!(f, "{}", self.vertical_symbol(&symbols, line_index, 0))?;
            let mut data_it = line.iter().enumerate().peekable();
            while let Some((column_index, data)) = data_it.next() {
                write!(
                    f,
                    "{}",
                    data.pad_to_width_with_alignment(sizes[column_index], pad::Alignment::Right)
                )?;
                if data_it.peek().is_some() {
                    write!(
                        f,
                        "{}",
                        self.vertical_symbol(&symbols, line_index, column_index)
                    )?;
                }
            }
            writeln!(
                f,
                "{}",
                self.vertical_symbol(&symbols, line_index, line.len() - 1)
            )?;
            if line_it.peek().is_some() {
                self.print_line(f, &sizes, &symbols, LineType::MIDDLE(line_index))?;
            }
        }
        self.print_line(f, &sizes, &symbols, LineType::LAST)?;

        Ok(())
    }
}

#[derive(Copy, Debug, Clone)]
pub enum LineType {
    FIRST,
    MIDDLE(usize),
    LAST,
}

// Print du header colonne
