pub const ASCII_VERTICAL: &str = "|";
pub const ASCII_HORIZONTAL: &str = "-";
pub const ASCII_CROSS: &str = "+";

#[derive(Debug, Clone)]
pub struct Set {
    pub vertical: &'static str,
    pub vertical_header: &'static str,
    pub horizontal: &'static str,
    pub horizontal_header: &'static str,
    pub top_right: &'static str,
    pub top_right_header: &'static str,
    pub top_left: &'static str,
    pub top_left_header: &'static str,
    pub bottom_right: &'static str,
    pub bottom_right_header: &'static str,
    pub bottom_left: &'static str,
    pub bottom_left_header: &'static str,
    pub vertical_left: &'static str,
    pub vertical_left_header: &'static str,
    pub vertical_left_last_line_header: &'static str,
    pub vertical_right: &'static str,
    pub vertical_right_header: &'static str,
    pub vertical_right_last_line_header: &'static str,
    pub horizontal_down: &'static str,
    pub horizontal_down_header: &'static str,
    pub horizontal_down_last_header: &'static str,
    pub horizontal_up: &'static str,
    pub horizontal_up_header: &'static str,
    pub horizontal_up_last_header: &'static str,
    pub cross: &'static str,
    pub cross_header: &'static str,
    pub cross_last_column_header: &'static str,
    pub cross_last_line_header: &'static str,
}

impl Set {
    pub fn get(&self, symbol: Symbol) -> &'static str {
        match (symbol.left, symbol.right,  symbol.up, symbol.down) {
            (Direction::NORMAL, Direction::NORMAL, Direction::NORMAL, Direction::NORMAL) => self.cross,
            (Direction::HEADER, Direction::HEADER, Direction::HEADER, Direction::HEADER) => self.cross_header,
            (Direction::HEADER, Direction::HEADER, Direction::HEADER, Direction::NORMAL) => self.cross_last_line_header,
            (Direction::HEADER, Direction::NORMAL, Direction::HEADER, Direction::HEADER) => self.cross_last_column_header,
            (Direction::NONE, Direction::NONE, Direction::NORMAL, Direction::NORMAL) => self.vertical,
            (Direction::NONE, Direction::NONE, Direction::HEADER, Direction::HEADER) => self.vertical_header,
            (Direction::NORMAL, Direction::NORMAL, Direction::NONE, Direction::NONE) => self.horizontal,
            (Direction::HEADER, Direction::HEADER, Direction::NONE, Direction::NONE) => self.horizontal_header,
            (Direction::NORMAL, Direction::NONE, Direction::NONE, Direction::NORMAL) => self.top_right,
            (Direction::HEADER, Direction::NONE, Direction::NONE, Direction::HEADER) => self.top_right_header,
            (Direction::NONE, Direction::NORMAL, Direction::NONE, Direction::NORMAL) => self.top_left,
            (Direction::NONE, Direction::HEADER, Direction::NONE, Direction::HEADER) => self.top_left_header,
            (Direction::NORMAL, Direction::NONE, Direction::NORMAL, Direction::NONE) => self.bottom_right,
            (Direction::HEADER, Direction::NONE, Direction::HEADER, Direction::NONE) => self.bottom_right_header,
            (Direction::NONE, Direction::NORMAL, Direction::NORMAL, Direction::NONE) => self.bottom_left,
            (Direction::NONE, Direction::HEADER, Direction::HEADER, Direction::NONE) => self.bottom_left_header,
            (Direction::NORMAL, Direction::NONE, Direction::NORMAL, Direction::NORMAL) => self.vertical_left,
            (Direction::HEADER, Direction::NONE, Direction::HEADER, Direction::HEADER) => self.vertical_left_header,
            (Direction::HEADER, Direction::NONE, Direction::HEADER, Direction::NORMAL) => self.vertical_left_last_line_header,
            (Direction::NONE, Direction::NORMAL, Direction::NORMAL, Direction::NORMAL) => self.vertical_right,
            (Direction::NONE, Direction::HEADER, Direction::HEADER, Direction::HEADER) => self.vertical_right_header,
            (Direction::NONE, Direction::HEADER, Direction::HEADER, Direction::NORMAL) => self.vertical_right_last_line_header,
            (Direction::NORMAL, Direction::NORMAL, Direction::NONE, Direction::NORMAL) => self.horizontal_down,
            (Direction::HEADER, Direction::HEADER, Direction::NONE, Direction::HEADER) => self.horizontal_down_header,
            (Direction::HEADER, Direction::NORMAL, Direction::NONE, Direction::HEADER) => self.horizontal_down_last_header,
            (Direction::NORMAL, Direction::NORMAL, Direction::NORMAL, Direction::NONE) => self.horizontal_up,
            (Direction::HEADER, Direction::HEADER, Direction::HEADER, Direction::NONE) => self.horizontal_up_header,
            (Direction::HEADER, Direction::NORMAL, Direction::HEADER, Direction::NONE) => self.horizontal_up_last_header,
            _ => " "
        }
    }
}

#[derive(Copy, Debug, Clone)]
pub struct Symbol {
    pub left: Direction,
    pub right: Direction,
    pub down: Direction,
    pub up: Direction,
}

#[derive(Copy, Debug, Clone)]
pub enum Direction{
    NORMAL,
    HEADER,
    NONE
}


// Ajout des différents symbol dans le set
// Ajout d’un fonction dans le set qui donne le symbol &str en fonction de l’enum

pub const NORMAL: Set = Set {
    vertical: "│",
    vertical_header: "┃",
    horizontal: "─",
    horizontal_header: "━",
    top_right: "┐",
    top_right_header: "┓",
    top_left: "┌",
    top_left_header: "┏",
    bottom_right: "┘",
    bottom_right_header: "┛",
    bottom_left: "└",
    bottom_left_header: "┗",
    vertical_left: "┤",
    vertical_left_header: "┫",
    vertical_left_last_line_header: "┩",
    vertical_right: "├",
    vertical_right_header: "┣",
    vertical_right_last_line_header: "┡",
    horizontal_down: "┬",
    horizontal_down_header: "┳",
    horizontal_down_last_header: "┱",
    horizontal_up: "┴",
    horizontal_up_header: "┻",
    horizontal_up_last_header: "┹",
    cross: "┼",
    cross_header: "╋",
    cross_last_line_header: "╇",
    cross_last_column_header: "╉",
};

pub const ASCII: Set = Set {
    vertical: ASCII_VERTICAL,
    vertical_header: ASCII_VERTICAL,
    horizontal: ASCII_HORIZONTAL,
    horizontal_header: ASCII_HORIZONTAL,
    top_right: ASCII_CROSS,
    top_right_header: ASCII_CROSS,
    top_left: ASCII_CROSS,
    top_left_header: ASCII_CROSS,
    bottom_right: ASCII_CROSS,
    bottom_right_header: ASCII_CROSS,
    bottom_left: ASCII_CROSS,
    bottom_left_header: ASCII_CROSS,
    vertical_left: ASCII_CROSS,
    vertical_left_header: ASCII_CROSS,
    vertical_left_last_line_header: ASCII_CROSS,
    vertical_right: ASCII_CROSS,
    vertical_right_header: ASCII_CROSS,
    vertical_right_last_line_header: ASCII_CROSS,
    horizontal_down: ASCII_CROSS,
    horizontal_down_header: ASCII_CROSS,
    horizontal_down_last_header: ASCII_CROSS,
    horizontal_up: ASCII_CROSS,
    horizontal_up_header: ASCII_CROSS,
    horizontal_up_last_header: ASCII_CROSS,
    cross: ASCII_CROSS,
    cross_header: ASCII_CROSS,
    cross_last_line_header: ASCII_CROSS,
    cross_last_column_header: ASCII_CROSS,
};
