use anyhow::{anyhow, Result};
use calamine::Reader;
use std::fs::File;
use std::path::Path;

pub struct Csv {
    iterator: csv::StringRecordsIntoIter<File>,
}

impl Csv {
    pub fn new<P: AsRef<Path>>(path: P, delimiter: char) -> Result<Self> {
        let reader = csv::ReaderBuilder::new()
            .delimiter(delimiter as u8)
            .has_headers(false)
            .from_path(path.as_ref())?;
        let iterator = reader.into_records();
        Ok(Csv { iterator })
    }
}
impl Iterator for Csv {
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(Ok(records)) = self.iterator.next() {
            Some(
                records
                    .into_iter()
                    .map(|record| record.to_string())
                    .collect(),
            )
        } else {
            None
        }
    }
}

pub struct Spreadsheets {
    range: calamine::Range<calamine::DataType>,
    line_index: u32,
}

impl Spreadsheets {
    pub fn new<P: AsRef<Path>>(path: P, sheet_name: Option<&str>) -> Result<Self> {
        let mut workbook = calamine::open_workbook_auto(path)?;
        let range = if let Some(sheet_name) = sheet_name {
            workbook
                .worksheet_range(sheet_name)
                .ok_or(anyhow!("Cannot find '{}'", sheet_name))??
        } else {
            workbook.worksheet_range_at(0).unwrap()?
        };
        Ok(Spreadsheets {
            range,
            line_index: 0,
        })
    }
}

impl Iterator for Spreadsheets {
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut res = Vec::new();
        let mut column_index = 0;
        while let Some(data) = self.range.get_value((self.line_index, column_index)) {
            res.push(data.to_string());
            column_index += 1;
        }
        self.line_index += 1;
        if res.is_empty() {
            None
        } else {
            Some(res)
        }
    }
}
